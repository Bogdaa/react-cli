const path = require('path')

module.exports = (componentName,defaultConfig) => ({
    'main': {
        fileName: `index.${defaultConfig.language}x`,
        templatePath: path.resolve(__dirname, './mainFile.js'),
    },
    'styles':{
        fileName: `styles.js`,
        templatePath: path.resolve(__dirname, './stylesFile.js'),
    },
})