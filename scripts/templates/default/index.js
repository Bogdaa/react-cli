const path = require('path')

module.exports = (componentName,defaultConfig) => ({
    'main': {
        fileName: `${componentName}.${defaultConfig.language}x`,
        templatePath: path.resolve(__dirname, './indexFile.js'),
    },
    'styles':{
        fileName: `${componentName}.${defaultConfig.styles}`,
        templatePath: 'none'
    },
    'index':{
        fileName: `index.${defaultConfig.language}`,
        templatePath: path.resolve(__dirname, './mainFile.js'),
    }
})

