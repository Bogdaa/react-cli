const path = require('path')

module.exports = (componentName,defaultConfig) => ({
    'main': {
        fileName: `${componentName}.${defaultConfig.language}x`,
        templatePath: path.resolve(__dirname, './mainFile.js'),
    },
    'styles':{
        fileName: `styles.${defaultConfig.language}`,
        templatePath: path.resolve(__dirname,'./stylesFile.js')
    },
    'index':{
        fileName: `index.${defaultConfig.language}`,
        templatePath: path.resolve(__dirname, './indexFile.js'),
    }
})