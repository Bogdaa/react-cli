const path = require('path');
const getDefaultTemplate = require('../templates/default');

const defaultConfig  = {
    styles: 'css',
    language:'js',
    basePath:'./src',
    defaultTemplate:getDefaultTemplate,
    types:{},
}

module.exports = defaultConfig;

