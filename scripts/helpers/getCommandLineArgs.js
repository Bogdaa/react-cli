const getCommandLineArgs = () => {
    return Object.fromEntries(process
        .argv
        .slice(2,process.argv.length)
        .map(cliArg => {
            const splitted = cliArg.split(':')
            return [splitted[0],splitted[1]]
        }))
}

module.exports = getCommandLineArgs;