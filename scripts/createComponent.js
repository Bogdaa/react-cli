const fs = require('fs');
const path = require('path');
const argv = require('./helpers/getCommandLineArgs')();
const isUserConfigExists = fs.existsSync(path.resolve(__dirname,'../react-cli.config.js'))
const config =
    Object.assign({},require('./constants/defaultConfig'),isUserConfigExists ? require('../react-cli.config.js') : {})

const { name: names, dirname } = argv;

const type = config.types[argv.type]

const templateConfig = {
    dirname:(type && type.dirname) || dirname,
    createTemplate:(type && type.template) || config.defaultTemplate
}


path
    .join(config.basePath,templateConfig.dirname)
    .split('/')
    .reduce((mainFolder,childFolder) => {
        const currentPath = path.resolve(__dirname,'..',mainFolder,childFolder)
        if(fs.existsSync(currentPath)) {

        } else {
            fs.mkdirSync(currentPath)
        }

        return `${mainFolder}/${childFolder}`
    },'./')


names
    .split(',')
    .filter(name => name !== '')
    .forEach(name => {
            const directoryPath = path.resolve(__dirname,'..',config.basePath,`${templateConfig.dirname}/${name}`)

            fs.mkdirSync(directoryPath)

            Object.entries(templateConfig.createTemplate(name,config))
                .forEach(([ ,template ]) => {
                    const { templatePath,fileName } = template;
                    let fileContent = '';
                    if(templatePath !== 'none') {
                        const fileTemplate = fs.readFileSync(templatePath,'utf-8')
                        fileContent = fileTemplate
                            .replace(/COMPONENT_NAME/ig,name)
                            .replace(/css/ig,config.styles)
                    }

                    fs.writeFileSync(`${directoryPath}/${fileName}`,fileContent)
                })
    })





