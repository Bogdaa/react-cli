const path = require('path')
const getDefaultTemplate = require('./scripts/templates/default')
const getMUITemplate = require('./scripts/templates/material-ui')
const getCustomTemplate = require('./scripts/templates/custom')

module.exports = {
    styles: 'scss',
    basePath:'./src',
    language:'ts',
    defaultTemplate:getCustomTemplate,
    types:{
        'UI':{
            dirname:'components/UI',
        },
        'common':{
            dirname: 'components/common',
        },
        'shared':{
            dirname:'components/shared',
        }
    }
}

